//
//  ViewController.swift
//  Har App Life Cycle
//
//  Created by Haryanto Salim on 19/05/21.
//

import UIKit

class ViewController: UIViewController {

    var firstNameLabel: UILabel?
    var lastNameLabel: UILabel?
    var nextPageButton: UIButton?
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //there are 2 labels with text properties named 'First Name' and 'Last Name'
        
        
        //there are 2 textfield of placeholder properties named 'first name' and 'last name'
        
        
        //there is 1 button to go to the next page
        
        for element in view.subviews{
            print(element)
            if let firstName = element as? UILabel{
                if firstName.text == "First Name"{
                    self.firstNameLabel = firstName
                }
            }
            if let lastName = element as? UILabel{
                if lastName.text == "Last Name"{
                    self.lastNameLabel = lastName
                }
            }
            if let button = element as? UIButton{
                if button.titleLabel?.text == "Go to Next Page"{
                    self.nextPageButton = button
                    button.addTarget(self, action: #selector(nextButtonTapped(_:)), for: .touchUpInside)
                }
            }
            
        }
        
        firstNameLabel?.text = "Recognized First Name Label"
        
        
        print(#function)
    }
    
    @objc func nextButtonTapped(_ sender: UIButton){
        lastNameLabel?.text = "button is tapped"
        prepareAndDisplayAlert()
    }
    
    func prepareAndDisplayAlert(){
        let alert = UIAlertController(title: "My Alert", message: "Message of the alert", preferredStyle: .alert)
//        let action = UIAlertAction(title: "OK", style: .default) { (action) in
//            print("done create action: \(String(describing: action.title))")
//        }
        let action = UIAlertAction(title: "OK", style: .default) { (action) in
            
            self.appDelegate?.scheduleNotification(notificationType: "Testing Notification")
            print("done schedule notif")
        }
        alert.addAction(action)
        self.present(alert, animated: true) {
            print("done presenting alert")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

        print(#function)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        print(#function)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        print(#function)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        print(#function)
    }
    
    override func loadView() {
        super.loadView()
        print(#function)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        print(#function)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        print(#function)
    }
}

